import React from 'react';
import PropTypes from 'prop-types';
import {Platform, TouchableNativeFeedback, View} from "react-native";

const Button = ({children, onPress, rippleColor, style}) => {


    return (
        <View style={{backgroundColor: '#ccc'}}>
            <TouchableNativeFeedback
                onPress={onPress}
                background={Platform['Version'] >= 21 ? TouchableNativeFeedback.Ripple(rippleColor || '#aaa', true) : TouchableNativeFeedback.SelectableBackground()}
                useForeground={false}
                borderless={true}
            >
                <View style={[styles.childContainer, style]}>
                    {children}
                </View>
            </TouchableNativeFeedback>
        </View>
    )
};

const styles = {
    childContainer: {
        width: '100%',
    }
};

Button.defaultProps = {
    children: null,
    onPress: () => {
    },
};

Button.propTypes = {
    children: PropTypes.node,
    onPress: PropTypes.func,
    rippleColor: PropTypes.string,
    style: PropTypes.object
};

export default Button;