import React from 'react';
import {Text} from 'react-native';

import {storiesOf} from '@storybook/react-native';
import {action} from '@storybook/addon-actions';
import {linkTo} from '@storybook/addon-links';

import Button from './Button';
import CenterView from './CenterView';
import Welcome from './Welcome';

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')}/>);

storiesOf('Button', module)
    .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
    .add('default-button', () => (
        <Button style={styles.defaultButton} onPress={action('default-button')}>
            <Text style={styles.defaultText}>Hello Button</Text>
        </Button>
    ));


const styles = {
    defaultButton: {
        backgroundColor: 'green',
        padding: 8
    },
    defaultText: {
        color: '#fff'
    }
};