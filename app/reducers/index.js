import {combineReducers} from "redux";
import MainReducer from "./MainReducer";
import NavReducer from "./NavReducer";
import AuthReducer from "./AuthReducer";
import QuizReducer from "./QuizReducer";
import FriendShipReducer from "./FriendShipReducer";
import FeedReducer from "./FeedReducer";

export default combineReducers({
    main: MainReducer,
    nav: NavReducer,
    auth: AuthReducer,
    quiz: QuizReducer,
    friendship: FriendShipReducer,
    feed: FeedReducer
});
