import {LOGGED_STATUS, UPDATED_USER} from "../actions/actionTypes";

const initialState = {
    logged: false,
    user: null,

};

export default (state = initialState, action) => {
    switch (action.type) {
    case LOGGED_STATUS:
        return {
            ...state,
            logged: action.payload
        };
    case UPDATED_USER:
        return {
            ...state,
            user: action.payload
        };
    default:
        return state;
    }
};
