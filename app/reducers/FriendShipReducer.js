import {FRIENDSHIP_LIST_UPDATED} from "../actions/actionTypes";

const initialState = {
    friends: [],
    next: "",
    previous: "",
};

export default (state = initialState, action) => {
    if (action.type === FRIENDSHIP_LIST_UPDATED) {
        // console.log("action.payload", action.payload);

        return {
            ...state,
            friends: action.payload.results || [],
            next: action.payload.next || "",
            previous: action.payload.previous || "",
        };
    }

    return state;
};
