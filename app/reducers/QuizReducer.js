import {QUIZ_CREATE, QUIZ_UPDATE, QUIZIES_LOADED} from "../actions/actionTypes";

const initialState = {
    quizies: null,
};

export default (state = initialState, action) => {
    const {payload, type} = action;

    switch (type) {
    case QUIZIES_LOADED:
        return {
            ...state,
            quizies: payload
        };
    case QUIZ_CREATE:
        return {
            ...state,
            quizies: [...state.quizies, payload]
        };
    case QUIZ_UPDATE:
        return {
            ...state,
            quizies: [...state.quizies.map((quiz) => (quiz.id === payload.id ? payload : quiz))]
        };
    default:
        return state;
    }
};
