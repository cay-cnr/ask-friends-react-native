import _ from "lodash";
import {FEEDS_LOADED, LIKED_FEED, NEW_FEEDS_LOADED_END, UNLIKED_FEED} from "../actions/actionTypes";

const initialState = {
    feeds: [],
    next: "",
    previous: "",
};

export default (state = initialState, action) => {
    if (action.type === FEEDS_LOADED) {
        // console.log("action.payload", action.payload);

        return {
            ...state,
            feeds: action.payload.results || [],
            next: action.payload.next || "",
            previous: action.payload.previous || "",
        };
    }
    else if (action.type === NEW_FEEDS_LOADED_END) {
        return {
            ...state,
            feeds: [...state.feeds, ...(action.payload.results || [])],
            next: action.payload.next || "",
            previous: action.payload.previous || "",
        };
    }
    else if (action.type === LIKED_FEED) {
        const feed = _.find(state.feeds, {id: action.feedId});
        feed.likes.push(action.payload);
        return {
            ...state,
            feeds: [...state.feeds],
        };
    }
    else if (action.type === UNLIKED_FEED) {
        const feed = _.find(state.feeds, {id: action.feedId});
        _.remove(feed.likes, {id: action.activityId});

        return {
            ...state,
            feeds: [...state.feeds],
        };
    }


    return state;
};
