import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {createReduxBoundAddListener} from "react-navigation-redux-helpers";
import MainRouter from "./routers/SwitchRouter";

const addListener = createReduxBoundAddListener("root");


class Router extends Component {
	render() {
		const {dispatch, nav} = this.props;
		return (
			<View style={{flex: 1}}>
				<MainRouter
					navigation={{
						dispatch: dispatch,
						state: nav,
						addListener
					}}
				/>
			</View>
		);
	}
}

Router.propTypes = {
	dispatch: PropTypes.instanceOf(Object),
	nav: PropTypes.instanceOf(Object)
};

const mapStateToProps = state => {
	return {
		nav: state.nav,
	};
};

export default connect(mapStateToProps)(Router);
