import React from "react";
import {connect} from "react-redux";
import {createOrUpdateQuiz} from "../../actions/quizies";
import {Text, View} from "react-native";

class AddQuiz extends React.Component {

    state = {
        quiz: null
    };

    render() {
        return (
            <View>
                <View noShadow={true}>
                    <Text>Create Quiz</Text>
                </View>
                <View>
                    {/*<Form>
                        <Item stackedLabel>
                            <Label>Title</Label>
                            <Input/>
                        </Item>
                        <Item stackedLabel last>
                            <Label>Description</Label>
                            <Input/>
                        </Item>
                    </Form>*/}
                    <Button info onPress={() => {
                        this.props.createOrUpdateQuiz(this.state.quiz);
                    }}>
                        <Text>Create Quiz</Text>
                    </Button>
                </View>
            </View>
        );
    }

}


const mapStateToProps = ({quiz}) => {
    return {quiz}
};

const AddQuizRedux = connect(mapStateToProps, {createOrUpdateQuiz})(AddQuiz);
export {AddQuizRedux as AddQuiz};