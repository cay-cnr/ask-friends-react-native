import React from "react";
import {connect} from "react-redux";
import {createOrUpdateQuiz} from "../../actions/quizies";

class UpdateQuiz extends React.Component {

    state = {
        quiz: null
    };

    render() {
        return (
            <Container>
                <View noShadow={true}>
                    <View>
                        <Button transparent onPress={() => {
                            this.props.navigation.pop();
                        }}>
                        </Button>
                    </View>
                    <Body>
                    <Title>Create Quiz</Title>
                    </Body>
                </View>
                <Content>
                    <Form>
                        <Item stackedLabel>
                            <Label>Title</Label>
                            <Input/>
                        </Item>
                        <Item stackedLabel last>
                            <Label>Description</Label>
                            <Input/>
                        </Item>
                    </Form>
                    <Button info onPress={() => {
                        this.props.createOrUpdateQuiz(this.state.quiz);
                    }}>
                        <Text>Create Quiz</Text>
                    </Button>
                </Content>
            </Container>
        );
    }

}


const mapStateToProps = ({quiz}) => {
    return {quiz}
};

const UpdateQuizRedux = connect(mapStateToProps, {createOrUpdateQuiz})(UpdateQuiz);
export {UpdateQuizRedux as UpdateQuiz};