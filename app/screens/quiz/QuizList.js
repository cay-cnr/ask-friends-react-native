import React from "react";
import {connect} from "react-redux";
import {createOrUpdateQuiz, getQuizies} from "../../actions/quizies";
import {FlatList, Image, Text, View} from "react-native";
import {Icon} from "../../components";
import {Button} from "../../components/button/common";
import PropTypes from "prop-types";


class QuizList extends React.Component {

    componentWillMount() {
        this.props.getQuizies();
    }

    renderItem = ({item}) => (
        <View style={styles.itemContainer}>
            <View style={styles.titleContainer}>
                <Text style={styles.titleText} numberOfLines={2}>{item.title}</Text>
                <View style={styles.statusContainer}>
                    <Icon
                        style={styles.statusIcon}
                        size={21}
                        color="#fff"
                        name="md-apps"
                    />
                    <Text style={styles.statusText}>Solved</Text>
                </View>
            </View>

            <View style={styles.contentContainer}>
                <View style={styles.buttonContainer}>
                    <Button
                        style={styles.buttonStyle} onPress={() => {

                    }}>
                        <Icon name="md-heart" color="#F95862"/>
                        <Text style={styles.likeText}>Like</Text>
                    </Button>
                    <Button
                        style={styles.buttonStyle} onPress={() => {

                    }}>
                        <Icon name="ios-chatboxes-outline" color="#8989A1"/>
                        <Text style={styles.commentText}>Comment</Text>
                    </Button>
                </View>
                <View style={styles.likerContainer}>
                    <Image
                        style={[styles.avatarImage]}
                        source={{uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQY4fUt8YEFQ8pDYuwIpc-IreQ4fquea8bscR9VY_FjpjD69Q2"}}
                    />
                    <Image
                        style={[styles.avatarImage, {marginLeft: -30}]}
                        source={{uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQY4fUt8YEFQ8pDYuwIpc-IreQ4fquea8bscR9VY_FjpjD69Q2"}}
                    />
                    <Image
                        style={[styles.avatarImage, {marginLeft: -30}]}
                        source={{uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQY4fUt8YEFQ8pDYuwIpc-IreQ4fquea8bscR9VY_FjpjD69Q2"}}
                    />
                    <Image
                        style={[styles.avatarImage, {marginLeft: -30}]}
                        source={{uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQY4fUt8YEFQ8pDYuwIpc-IreQ4fquea8bscR9VY_FjpjD69Q2"}}
                    />
                    <View style={{
                        width: 50,
                        height: 50,
                        borderRadius: 25,
                        backgroundColor: "#000",
                        opacity: 0.5,
                        marginLeft: -50,
                        alignItems: "center",
                        justifyContent: "center"
                    }}>
                        <Text style={{color: "#fff"}}>+25</Text>
                    </View>
                </View>
            </View>

            <View/>
        </View>
    );

    render() {
        return (
            <View style={{
                backgroundColor: "#F9F9F9",
                flex: 1
            }}>
                <View style={styles.headContainer}>
                    <Text style={styles.headText}>Top Popular Quizs</Text>
                    <Button
                        style={styles.createButton} onPress={() => {

                    }}>
                        <Text style={styles.createText}>Create</Text>
                    </Button>
                </View>
                <FlatList
                    data={this.props.quizies}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

QuizList.propTypes = {
    getQuizies: PropTypes.func,
    quizies: PropTypes.object

};

const styles = {
    headContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        margin: 15
    },
    createText: {
        color: "#fff",

    },
    headText: {
        fontSize: 18,
    },
    createButton: {
        backgroundColor: "#5993FF",
        borderRadius: 48,
        padding: 15,
        minWidth: 130,
        alignItems: "center",
        justifyContent: "center"
    },
    itemContainer: {
        margin: 15,
        marginBottom: 0,
        borderColor: "#EBEBEB",
        borderWidth: 1,
        borderRadius: 4,
        backgroundColor: "#fff",
        flex: 1,
        elevation: 1,
    },
    titleContainer: {
        marginLeft: 15,
        marginTop: 15,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    titleText: {
        fontSize: 17
    },
    statusContainer: {
        padding: 8,
        flexDirection: "row",
        backgroundColor: "#ff7722",
        borderBottomLeftRadius: 50,
        borderTopLeftRadius: 50,
        alignItems: "center",
    },
    avatarImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
        shadowColor: "#4b4b4b",
        shadowOffset: {
            width: 0,
            height: -1
        },
        shadowRadius: 2,
        shadowOpacity: 1,
        elevation: 1,
        resizeMode: "contain",
        marginLeft: -15,
    },
    statusText: {
        color: "#fff",
        marginRight: 8
    },
    statusIcon: {
        marginLeft: 8,
        marginRight: 8,
    },
    contentContainer: {
        margin: 15,
        flexDirection: "row",
        flex: 1
    },
    commentText: {
        color: "#8989A1",
        marginLeft: 6
    },
    likeText: {
        color: "#F95862",
        marginLeft: 6
    },
    buttonStyle: {
        flexDirection: "row",
        alignItems: "center",
        marginRight: 8,
        borderWidth: 1,
        borderRadius: 48,
        padding: 10
    },
    buttonContainer: {
        flexDirection: "row",
        flex: 1
    },
    likerContainer: {
        flexDirection: "row",
        width: 100
    }
};


const mapStateToProps = ({quiz}) => ({quizies: quiz.quizies});

const QuizListRedux = connect(mapStateToProps, {
    getQuizies,
    createOrUpdateQuiz
})(QuizList);
export {QuizListRedux as QuizList};
