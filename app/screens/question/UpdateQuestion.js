import React from "react";
import {connect} from "react-redux";
import {createOrUpdateQuestion} from "../../actions/quizies";

class UpdateQuestion extends React.Component {

    state = {
        question: null
    };

    render() {
        return (
            <Container>
                <Header noShadow={true}>
                    <Left>
                        <Button transparent onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                    <Title>Create Question</Title>
                    </Body>
                </Header>
                <Content>
                    <Form>
                        <Item stackedLabel>
                            <Label>Title</Label>
                            <Input/>
                        </Item>
                        <Item stackedLabel last>
                            <Label>Description</Label>
                            <Textarea/>
                        </Item>
                    </Form>
                    <Button style={{alignSelf: 'center'}} info onPress={() => {
                        this.props.createOrUpdateQuestion(this.state.question);
                    }}>
                        <Text>Create Quiz</Text>
                    </Button>
                </Content>
            </Container>
        );
    }

}


const mapStateToProps = ({quiz}) => {
    return {quiz}
};

const UpdateQuestionRedux = connect(mapStateToProps, {createOrUpdateQuestion})(UpdateQuestion);
export {UpdateQuestionRedux as UpdateQuestion};