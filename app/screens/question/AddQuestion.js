import React from "react";
import {connect} from "react-redux";
import {createOrUpdateQuestion} from "../../actions/quizies";
import {Text, View} from "react-native";

class AddQuestion extends React.Component {

    state = {
        question: null
    };

    render() {
        return (
            <View>
                <View noShadow={true}>
                    <Button transparent onPress={() => {
                        this.props.navigation.goBack();
                    }}>
                        <Icon name='arrow-back'/>
                    </Button>
                    <View>
                        <Text>Create Question</Text>
                    </View>
                </View>
                <Content>
                    {/*
                    <Form>
                        <Item stackedLabel>
                            <Label>Title</Label>
                            <Input/>
                        </Item>
                        <Item stackedLabel last>
                            <Label>Description</Label>
                            <Textarea/>
                        </Item>
                    </Form>
*/}
                    <Button style={{alignSelf: 'center'}} info onPress={() => {
                        this.props.createOrUpdateQuestion(this.state.question);
                    }}>
                        <Text>Create Quiz</Text>
                    </Button>
                </Content>
            </View>
        );
    }

}


const mapStateToProps = ({quiz}) => {
    return {quiz}
};

const AddQuestionRedux = connect(mapStateToProps, {createOrUpdateQuestion})(AddQuestion);
export {AddQuestionRedux as AddQuestion};