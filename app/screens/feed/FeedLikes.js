import React, {Component} from "react";
import {connect} from "react-redux";
import {FlatList, Text, View} from "react-native";
import PropTypes from "prop-types";

class FeedLikes extends Component {

    renderItem = ({item}) => (
        <View>
            <Text>{item.user.username}</Text>
        </View>
    );

    render() {
        const {likes} = this.props.navigation.state.params;

        return (
            <FlatList
                data={likes}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
        );
    }
}

FeedLikes.propTypes = {
    navigation: PropTypes.instanceOf(Object)
};

const mapStateToProps = () => ({});

const ReduxFeedLikes = connect(mapStateToProps)(FeedLikes);

export {ReduxFeedLikes as FeedLikes};
