import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {DrawerActions} from "react-navigation";
import {FlatList, Image, Platform, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import moment from "moment";
import _ from "lodash";

import {Icon} from "../../components";
import {Button} from "../../components/button/common";
import {getFeeds, likeFeed, unlikeFeed} from "../../actions/FeedActions";
import LikesContainer from "../../components/feed/LikesContainer";
import ListFooterComponent from "../../components/feed/ListFooterComponent";

const styles = {
    container: {
        marginTop: 5,
    },
    headContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 10
    },
    createText: {
        color: "#fff",
    },
    headText: {
        fontSize: 18,
    },
    createButton: {
        backgroundColor: "#5993FF",
        borderRadius: 48,
        minWidth: 130
    },
    insideButtonStyle: {
        alignItems: "center",
        justifyContent: "center",
        padding: 10
    },
    itemContainer: {
        borderColor: "#EBEBEB",
        borderBottomWidth: 1,
        borderRadius: 4,
        backgroundColor: "#fff",
        flex: 1,

    },
    postContainer: {
        marginLeft: 15,
        flexDirection: "row",
        justifyContent: "space-between",
        flex: 1
    },
    postText: {
        fontSize: 13,
        marginRight: 15,
        color: "#3f3f3f",
        flex: 1
    },
    statusContainer: {
        padding: 4,
        flexDirection: "row",
        backgroundColor: "#ff7722",
        borderBottomLeftRadius: 48,
        borderTopLeftRadius: 48,
        alignItems: "center",
        marginRight: -2,
        height: 35
    },
    avatarContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        flex: 1,
        padding: 15,

    },
    userCardContainer: {
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 15,
        flex: 1
    },
    nameTexts: {
        marginLeft: 10
    },
    userAvatarImage: {
        width: 40,
        height: 40,
        borderRadius: Platform.select({
            ios: 20,
            android: 100
        }),
    },
    avatarImageContainer: {
        marginLeft: -15,
        backgroundColor: "#ccc"
    },
    statusText: {
        fontSize: 12,
        color: "#fff",
        marginRight: 8
    },
    statusIcon: {
        marginLeft: 8,
        marginRight: 8,
    },
    contentContainer: {
        padding: 15,
        flexDirection: "row",
        flex: 1
    },
    commentText: {
        color: "#8989A1",
        marginLeft: 6,
        fontSize: 12
    },
    likeText: {
        color: "#F95862",
        marginLeft: 6,
        fontSize: 12
    },
    buttonStyle: {
        marginRight: 8,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#dddddd",
        borderRadius: 40,
    },
    buttonInsideStyle: {
        flexDirection: "row",
        width: 40,
        height: 40,
        alignItems: "center",
        justifyContent: "center"
    },
    buttonContainer: {
        flexDirection: "row",
        alignItems: "center",
        flex: 1
    },
    dateText: {
        fontSize: 12,
        alignSelf: "center",
        marginBottom: 4,
        fontWeight: "300",
        color: "#cccccc"
    },
    titleText: {
        color: "#050505",
        fontWeight: "500",
        fontSize: 14
    },
    subtitleText: {
        color: "#222",
        fontSize: 14
    }
};

class Feeds extends React.Component {
    static navigationOptions = ({navigation}) => (
        {
            headerTitle: (
                <View style={{
                    alignItems: "center",
                    flex: 1
                }}>
                    <Image
                        source={require("../../../assets/images/logo.png")}
                        style={{
                            width: 100,
                            resizeMode: "contain"
                        }}
                    />
                </View>
            ),
            headerTitleStyle: {
                flex: 1,
                alignSelf: "center"
            },
            headerLeft: (
                <TouchableOpacity onPress={() => {
                    navigation.toggleDrawer();
                }}>
                    <Image
                        source={require("../../../assets/icons/menu_icon.png")}
                        style={{
                            width: 15,
                            height: 15,
                            margin: 16,
                            resizeMode: "contain"
                        }}
                    />
                </TouchableOpacity>
            ),
            headerRight: (
                <TouchableOpacity onPress={() => {
                    navigation.navigate(DrawerActions.openDrawer());
                }}>
                    <Image
                        source={require("../../../assets/icons/settings_icon.png")}
                        style={{
                            width: 15,
                            height: 15,
                            resizeMode: "contain",
                            margin: 16
                        }}
                    />
                </TouchableOpacity>
            )
        }
    );

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            loadingEndData: false
        };
    }

    componentWillMount() {
        this.props.getFeeds();

    }

    onRefresh = () => {
        this.setState({refreshing: true});
        this.props.getFeeds()
            .then(() => {
                this.setState({refreshing: false});
            });
    };

    onEndReached = () => {
        const {next} = this.props;
        if (!this.state.loadingEndData && next) {
            this.setState({loadingEndData: true}, () => {
                this.props.getFeeds(next)
                    .then(() => {
                        this.setState({loadingEndData: false});
                    });
            });
        }
    };

    renderItem = ({item}) => (
        <View style={styles.container}>
            <View style={styles.itemContainer}>
                <View style={styles.avatarContainer}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate("Profile", {user: item.user});
                        }}
                        activeOpacity={0.9}
                        style={styles.userCardContainer}>
                        <View style={[styles.avatarImageContainer, styles.userAvatarImage]}>
                            <Image
                                style={[styles.avatarImage, styles.userAvatarImage]}
                                source={item.user.profile.photo ? {uri: item.user.profile.photo} : require("../../../assets/icons/avatar_placeholder.png")}
                            />
                        </View>
                        <View style={styles.nameTexts}>
                            <Text style={styles.titleText}>{`${item.user.first_name} ${item.user.last_name}`}</Text>
                            <Text style={styles.subtitleText}>{`@${item.user.username}`}</Text>
                        </View>
                    </TouchableOpacity>
                    <Button>
                        <Icon name="md-more" size={21}/>
                    </Button>
                </View>
                <View style={styles.postContainer}>
                    <Text style={styles.postText} numberOfLines={10}>{item.post}</Text>
                    {item.quiz.test_takers.filter((user) => this.props.user.id === user.id).length > 0 ? (
                        <View style={styles.statusContainer}>
                            <Icon
                                style={styles.statusIcon}
                                size={20}
                                color="#fff"
                                name="md-apps"
                            />
                            <Text style={styles.statusText}>Solved</Text>
                        </View>
                    ) : (
                        <View style={[styles.statusContainer, {backgroundColor: "#21a945"}]}>
                            <Icon
                                style={styles.statusIcon}
                                size={20}
                                color="#fff"
                                name="md-apps"
                            />
                            <Text style={styles.statusText}>Solve</Text>
                        </View>
                    )}
                </View>

                <View style={styles.contentContainer}>
                    <View style={styles.buttonContainer}>
                        <Button
                            style={styles.buttonStyle}
                            insideStyle={styles.buttonInsideStyle}
                            onPress={() => {
                                const like = _.find(item.likes, {user: {id: this.props.user.id}});

                                if (like) {
                                    this.props.unlikeFeed(like.id, item.id);
                                }
                                else {
                                    this.props.likeFeed(item.id);
                                }
                            }}>
                            <Icon
                                name={item.likes.filter((like) => like.user.id === this.props.user.id).length > 0 ? "ios-heart" : "ios-heart-outline"}
                                size={26}
                                color="#F95862"
                            />
                        </Button>
                        <Button
                            style={styles.buttonStyle}
                            insideStyle={styles.buttonInsideStyle}
                            onPress={() => {

                            }}>
                            <Icon name="ios-chatboxes-outline" size={26} color="#8989A1"/>
                        </Button>
                    </View>

                    <LikesContainer likes={item.likes}/>
                </View>

                <Text style={styles.dateText}>{
                    moment(item.created)
                        .fromNow()
                        .toUpperCase()
                }
                </Text>
            </View>
        </View>
    );

    render() {
        return (
            <View style={{
                backgroundColor: "#F9F9F9",
                flex: 1
            }}>
                <View style={styles.headContainer}>
                    <Text style={styles.headText}>Top Popular Quizs</Text>
                    <Button
                        style={styles.createButton}
                        insideStyle={styles.insideButtonStyle}
                        onPress={() => {

                        }}>
                        <Text style={styles.createText}>Create</Text>
                    </Button>
                </View>

                <FlatList
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    data={this.props.feeds}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => `${item.id.toString() + index.toString()}`}
                    onEndReachedThreshold={0.5}
                    onEndReached={this.onEndReached}
                    ListFooterComponent={() => (<ListFooterComponent loading={this.state.loadingEndData}/>)}
                />
            </View>
        );
    }
}

Feeds.propTypes = {
    feeds: PropTypes.instanceOf(Array),
    getFeeds: PropTypes.func,
    user: PropTypes.instanceOf(Object),
    navigation: PropTypes.instanceOf(Object),
    next: PropTypes.string,
    likeFeed: PropTypes.func,
    unlikeFeed: PropTypes.func
};

const mapStateToProps = ({feed, auth}) => ({...feed, ...auth});

const FeedsRedux = connect(mapStateToProps, {
    getFeeds,
    likeFeed,
    unlikeFeed
})(Feeds);

export {FeedsRedux as Feeds};
