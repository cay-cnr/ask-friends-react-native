import React from "react";
import {connect} from "react-redux";
import {Dimensions, FlatList, Text, View} from "react-native";
import {Button} from "../../components/button/common/";

const {width} = Dimensions.get("window");
const ITEM_HEIGHT = 50;
const styles = {
    bubble: {
        minHeight: 35,
        minWidth: 50,
        maxWidth: width / 1.5,
        borderRadius: 20,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10
    },

    myBubble: {
        alignSelf: "flex-end",
        marginRight: 20,
        borderBottomRightRadius: 5,
        backgroundColor: "#5756ea",
    },

    // Bubbles text

    bubbleText: {
        alignSelf: "center",
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: "transparent"
    },

    myBubbleText: {
        color: "#ffffff"
    },

    flatlistContainerStyle: {
        flexGrow: 1,
        justifyContent: "center"
    },
    container: {
        display: "flex",
        flexDirection: "column",
        height: "100%",
        backgroundColor: "#eeeeee"
    }
};

class ChatDetail extends React.Component {
    static navigationOptions = () => ({
        headerStyle: {
            elevation: 0,
            shadowOpacity: 0
        },
    });

    componentDidUpdate() {
        if (this.props.data.length) {
            this.flatList.scrollToIndex({
                animated: true,
                index: 0
            });
        }
    }

    takeNextMessages = () => {

    };

    itemLayout = (data, index) => (
        {
            length: ITEM_HEIGHT,
            offset: ITEM_HEIGHT * index,
            index
        }
    );

    renderMessage = ({item}) => (
        <View style={[styles.bubble, styles.myBubble]}>
            <Text style={[styles.bubbleText, styles.myBubbleText]}>{item.message}</Text>
        </View>
    );

    render() {
        const {title, messages} = this.props.navigation.state.params;
        const contentContainerStyle = messages.length ? null : styles.flatlistContainerStyle;
        // TODO basic chat page required

        return (
            <View>
                <View style={{borderBottomWidth: 1}} noShadow>
                    <Button
                        transparent
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                    />
                    <View>
                        <Text>{title}</Text>
                    </View>
                </View>
                <View>
                    <FlatList
                        contentContainerStyle={contentContainerStyle}
                        ref={(c) => {
                            this.flatList = c;
                        }}
                        inverted
                        style={styles.container}
                        data={messages}
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderMessage}
                        getItemLayout={this.itemLayout}
                        onEndReached={this.takeNextMessages}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = ({auth}) => ({user: auth.user});

const ChatDetailRedux = connect(mapStateToProps)(ChatDetail);
export {ChatDetailRedux as ChatDetail};
