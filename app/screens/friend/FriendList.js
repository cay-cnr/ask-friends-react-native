/* eslint-disable import/no-unresolved,import/prefer-default-export */
import React from "react";
import {FlatList, Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {getFriendList} from "../../actions/FriendshipActions";
import {SearchView} from "../../components/SearchView";
import {Icon} from "../../components";
import {Button} from "../../components/button/common/";

const styles = StyleSheet.create({
    headerTitle: {
        flex: 1,
        fontWeight: "400",
        fontSize: 18,
        textAlign: "center"
    },
    container: {
        flex: 1,
        alignSelf: "stretch",
        paddingTop: 20,
        backgroundColor: "#F9F9F9"
    },
    avatarImage: {
        width: 64,
        height: 64,
        borderRadius: 64,
        resizeMode: "contain",
        marginRight: 12
    },
    itemContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 15,
        marginRight: 15,
        flex: 1
    },
    username: {
        fontSize: 16,
        color: "#222"
    },
    lastMessage: {
        fontSize: 14,
        color: "#818181"
    },
    time: {
        fontSize: 15,
        color: "#818181",
        margin: 4
    },
    itemRightContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        flex: 1,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: "#a6aecb",
        paddingBottom: 12,
        paddingTop: 12
    },
    listContainer: {
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: "#a6aecb",
        flex: 1
    }
});

class FriendList extends React.Component {
    static navigationOptions = ({navigation}) => (
        {
            title: "Friends",
            headerTitleStyle: styles.headerTitle,
            headerLeft: (
                <TouchableOpacity onPress={() => {
                    navigation.toggleDrawer();
                }}>
                    <Image
                        source={require("../../../assets/icons/menu_icon.png")}
                        style={{
                            width: 15,
                            height: 15,
                            margin: 16,
                            resizeMode: "contain"
                        }}
                    />
                </TouchableOpacity>
            ),
            headerRight: (
                <View/>
            )
        }
    );

    constructor(props) {
        super(props);
        this.state = {
            searchTerm: ""
        };
    }

    componentWillMount() {
        this.props.getFriendList();
    }

    renderItem = ({item}) => {
        const {photo} = item.from_user.profile;
        const avatar = photo ? {uri: photo} : require("../../../assets/icons/avatar_placeholder.png");
        return (
            <View style={styles.itemContainer}>
                <Image
                    source={avatar}
                    style={styles.avatarImage}
                />
                <View style={styles.itemRightContainer}>
                    <View style={{flex: 1}}>
                        <Text numberOfLines={1} style={styles.username}>{item.from_user.username}</Text>
                        <Text numberOfLines={2} style={styles.lastMessage}>{item.last_message[0].message}</Text>
                        <Text style={styles.time}>Now</Text>
                    </View>

                    <View style={{
                        width: 30,
                        flexDirection: "column",
                        alignItems: "flex-end",
                    }}>
                        <Image resizeMode="contain" source={require("../../../assets/icons/right_arrrow.png")}/>
                    </View>
                </View>
            </View>
        );
    };


    render() {
        return (
            <View style={{flex: 1}}>
                <SearchView
                    style={{
                        color: "#303030",
                        padding: 5
                    }}
                    value={this.state.searchTerm}
                    onChange={(searchTerm) => {
                        this.setState({searchTerm});
                    }}
                />
                <FlatList
                    style={styles.listContainer}
                    data={this.props.friends}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
                <Button
                    style={{
                        backgroundColor: "#5993FF",
                        height: 50
                    }}
                    insideStyle={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        padding: 15
                    }}>
                    <Icon name="ios-chatboxes" size={18} color="#fff"/>
                    <Text style={{
                        marginLeft: 5,
                        color: "#fff"
                    }}>
                        New Message
                    </Text>
                </Button>
            </View>

        );
    }
}

const mapStateToProps = ({friendship}) => ({...friendship});

FriendList.propTypes = {
    getFriendList: PropTypes.func,
    friends: PropTypes.instanceOf(Object)
};

const FriendListRedux = connect(mapStateToProps, {getFriendList})(FriendList);
export {FriendListRedux as FriendList};
