import React from "react";
import {connect} from "react-redux";
import {Text, View} from "react-native";
import PropTypes from "prop-types";

import {LoginUser} from "../../actions/auth";
import {DefaultButton} from "../../components/";
import {Input} from "../../components/Input";

class Login extends React.Component {
    state = {
        username: "caner",
        password: "mku7lbcsv"
    };

    render() {
        const {username, password} = this.state;

        return (
            <View>
                <View style={{padding: 30}}>
                    <View>
                        <View>
                            <Input
                                title="Username"
                                onChange={(name) => this.setState({username: name})}
                            />
                        </View>
                        <View title="Password">
                            <Input
                                onChange={(pass) => this.setState({password: pass})}
                                secureTextEntry
                            />
                        </View>
                    </View>
                    <View style={{
                        flexDirection: "row",
                        flex: 1,
                        justifyContent: "flex-end"
                    }}>
                        <DefaultButton title="Forgot Password?"/>
                    </View>

                    <DefaultButton
                        title="Sign in"
                        onPress={() => {
                            this.props.LoginUser({
                                username,
                                password
                            });

                            // this.props.navigation.navigate('Home');
                        }}>
                        <Text>Continue</Text>
                    </DefaultButton>
                    <DefaultButton
                        title="Register"
                        onPress={() => {
                            this.props.navigation.navigate("Register");
                        }}
                    />
                </View>
                <DefaultButton title="Login with Facebook" style={{backgroundColor: "#007aff"}}/>
            </View>
        );
    }
}

const mapStateToProps = (state) => state;

Login.propTypes = {
    navigation: PropTypes.instanceOf(Object)
};

const LoginRedux = connect(mapStateToProps, {LoginUser})(Login);
export {LoginRedux as Login};
