import React from "react";
import {connect} from "react-redux";
import {Text, View} from "react-native";

class Settings extends React.Component {
    componentWillMount() {
    }

    render() {
        return (
            <View>
                <Text>Settings</Text>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return state
};

const SettingsRedux = connect(mapStateToProps)(Settings);
export {SettingsRedux as Settings};
