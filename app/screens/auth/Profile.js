import React from "react";
import {connect} from "react-redux";
import {Text, View} from "react-native";

class Profile extends React.Component {
    componentWillMount() {
    }

    render() {
        return (
            <View>
                <Text>Profile</Text>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

const ProfileRedux = connect(mapStateToProps)(Profile);
export {ProfileRedux as Profile};
