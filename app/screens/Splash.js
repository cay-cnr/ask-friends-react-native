import React from "react";
import {Text, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import {UserIsAuthenticated} from "../actions/auth";

class Splash extends React.Component {
    componentWillMount() {
        this.props.UserIsAuthenticated();
    }

    render() {
        return (
            <View>
                <Text>
                    Splash Page
                </Text>
            </View>
        );
    }
}

const mapStateToProps = (state) => state;

Splash.propTypes = {
    UserIsAuthenticated: PropTypes.func
};

const SplashRedux = connect(mapStateToProps, {UserIsAuthenticated})(Splash);
export {SplashRedux as Splash};
