import {createStackNavigator} from "react-navigation";
import colorConfig from "../../configs/colorConfig";
import {ForgetPassword, Login, Register} from "../screens";

export const AuthNavigator = createStackNavigator({
    Login: {
        screen: Login,
    },
    Register: {
        screen: Register,
    },
    ForgetPassword: {
        screen: ForgetPassword,
    }
}, {
    cardStyle: {
        backgroundColor: colorConfig().DEFAULT_BACKGROUND_COLOR
    },
    initialRouteName: "Login"
});
