import {createSwitchNavigator} from "react-navigation";
import {Splash} from "../screens";
import {AuthNavigator} from "./AuthRouter";
import drawerNavigator from "./DrawerNavigator";

export default createSwitchNavigator({
    AuthLoading: Splash,
    App: drawerNavigator,
    Auth: AuthNavigator,
}, {
    initialRouteName: "AuthLoading",
});
