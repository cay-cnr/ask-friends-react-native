import {Dimensions, StyleSheet} from "react-native";
import {createDrawerNavigator, createStackNavigator} from "react-navigation";

import routeConfig from "../../configs/routeConfig";
import {SideBar} from "../components/drawer";
import {AddQuestion, ChatDetail, FeedLikes} from "../screens";

const {width} = Dimensions.get("window");

const defaultDrawerNavigatorOptions = {
    contentComponent: SideBar,
    initialRouteName: "main",
    drawerWidth: width
};

const defaultDrawerStack = {
    AddQuestion,
    ChatDetail,
    FeedLikes,
};

routeConfig()
    .ROUTE_PAGES
    .forEach((route) => {
        defaultDrawerStack[route.routeName] = {
            screen: route.screen
        };
    });

const stackNavigator = createStackNavigator(defaultDrawerStack, {
    headerMode: "screen",
    navigationOptions: {
        headerStyle: {
            backgroundColor: "#FAFBFF",
            elevation: 0,
            borderColor: "#bfbfbf",
            borderBottomWidth: StyleSheet.hairlineWidth
        },
    },
    cardStyle: {
        backgroundColor: "#F9F9F9"
    },
    initialRouteName: "Home"
});
const drawerNavigator = createDrawerNavigator({main: stackNavigator}, defaultDrawerNavigatorOptions);

export default drawerNavigator;
