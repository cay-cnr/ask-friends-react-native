import ProfileSchema from "./ProfileSchema";
import UserSchema from "./UserSchema";
import Realm from 'realm';

export const realm = new Realm({schema: [ProfileSchema, UserSchema]});