export default {
    name: 'User',
    primaryKey: 'id',
    properties: {
        date_joined: {type: 'date'},
        email: {type: 'string'},
        first_name: {type: 'string'},
        friendship_type: {type: 'int'},
        id: {type: 'int'},
        is_active: {type: 'bool',},
        is_staff: {type: 'bool',},
        is_superuser: {type: 'bool',},
        last_login: {type: 'date'},
        last_name: {type: 'string'},
        profile: 'Profile',
        username: {type: 'string'}
    }
};