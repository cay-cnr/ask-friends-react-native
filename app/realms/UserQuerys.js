import {realm} from "./index";

export const createOrUpdateUser = (user) => {
    realm.write(() => {
        realm.create('User', user, true);
    });
};

export const findUserById = (id) => {
    return realm.objectForPrimaryKey('User', id);
};