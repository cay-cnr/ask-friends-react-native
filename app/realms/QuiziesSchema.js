// import {realm} from "./index";

export default {
    name: 'Quizies',
    primaryKey: 'id',
    properties: {
        id: {type: 'int'},
        author: 'User',
        title: {type: 'string'},
        description: {type: 'string'},
        status: {type: 'int'},
        publish: {type: 'date'},
        created: {type: 'date'},
        modified: {type: 'date'},
        test_takers: {type: 'list', objectType: 'User'}

    }
};


export const findQuizById = (id) => {
    // return realm.objectForPrimaryKey('Quizies', id);
};

export const getQuiziesByAuthorId = (id) => {
    // return realm.objects('Quizies').filtered(`author.id=${id}`);
};

export const createOrUpdateQuiz = (quiz) => {
    // realm.write(() => {
    //     realm.create('Quizies', quiz, true);
    // });
};

export const createOrUpdateQuizies = (quizies) => {
    // realm.write(() => {
    //     realm.create('Quizies', quizies, true)
    // })
};