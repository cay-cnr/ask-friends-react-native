export default {
    name: 'Profile',
    properties: {
        id: {type: 'int'},
        photo: {type: 'string'},
        firebase_key: {type: 'string'}
    }
};