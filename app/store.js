import {applyMiddleware} from "redux";
import {persistStore} from "redux-persist";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
import {createReactNavigationReduxMiddleware} from "react-navigation-redux-helpers";
import Reactotron from "../ReactotronConfig";

const middleware = createReactNavigationReduxMiddleware(
	"root",
	state => state.nav,
);

export default function configureStore(onComplete) {
	const store = Reactotron.createStore(
		rootReducer,
		applyMiddleware(thunk, middleware),
	);

	persistStore(
		store,
		onComplete
	);

	return store;
}