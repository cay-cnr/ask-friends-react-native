import React from "react";
import {StyleSheet, Text, TextInput, View} from "react-native";
import PropTypes from "prop-types";

const styles = {
    input: {
        borderBottomWidth: StyleSheet.hairlineWidth
    }
};

const Input = (props) => (
    <View>
        <Text>{props.title}</Text>
        <TextInput
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder={props.title}
            {...props}
            style={[styles.input, props.style]}
        />
    </View>
);

Input.propTypes = {
    title: PropTypes.string,
    style: PropTypes.instanceOf(Object)
};

export {Input};
