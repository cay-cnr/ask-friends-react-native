import React from "react";
import {Dimensions, Image, PixelRatio, Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import routeConfig from "../../../configs/routeConfig";
import {LogoutUser} from "../../actions/auth";
import {Button} from "../button/common/";
import {Icon} from "../";
import {API_URL} from "../../../configs/colorConfig";


const {width, height} = Dimensions.get("window");

const styles = {
    content: {
        marginTop: 50,
        marginBottom: 50,
    },
    routeContent: {
        paddingTop: 50,
        paddingBottom: 50,
        alignItems: "center",
    },
    container: {
        flex: 1,
        height,
        width,
    },
    routeContainer: {
        flex: 1,
        marginLeft: 15,
        marginTop: 100
    },
    footerFontStyle: {
        fontSize: 15,
        color: "#a8a8a8"
    },
    buttonStyle: {
        flex: 1
    },
    insideButtonStyle: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    TabContainerStyle: {
        paddingTop: 10,
        paddingBottom: 10,
        borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        borderColor: "#d3d3d3",
        position: "absolute",
        bottom: 0,
        width: "100%",
        flexDirection: "row",
    },
    line: {
        width: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        height: "100%",
        backgroundColor: "#d3d3d3"
    },
    routeItemStyle: {
        fontSize: 17,
        fontWeight: "400"
    },
    routeItemContainer: {
        flexDirection: "row",
        marginTop: 10
    },
    avatarImage: {
        marginTop: 10,
        width: 64,
        height: 64,
        resizeMode: "contain",
        borderRadius: 64,
        zIndex: 1111,
        alignSelf: "center"
    }
};

class SideBar extends React.Component {

    _getRoutes = () => routeConfig()
        .ROUTE_PAGES
        .map((item) => (
            <TouchableOpacity
                key={item.routeName}
                onPress={() => {
                    this.props.navigation.navigate(item.routeName, {
                        type: "REPLACE"
                    });
                }
                }>
                <View style={styles.routeItemContainer}>
                    <Text style={styles.routeItemStyle}>{item.name}</Text>
                </View>
            </TouchableOpacity>
        ));

    render() {
        const {profile, username} = this.props.user;

        const {photo} = profile;
        const avatar = photo ? {uri: API_URL + photo} : require("../../../assets/icons/avatar_placeholder.png");
        // console.log("avatar", avatar);

        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <Image
                        source={avatar}
                        style={styles.avatarImage}
                    />

                    <View style={{alignItems: "center"}}>

                        <Text style={{
                            fontSize: 21,
                            marginTop: 5,
                            fontWeight: "400"
                        }}>
                            {username}
                        </Text>
                        <View style={styles.routeContent}>
                            {this._getRoutes()}
                        </View>
                    </View>
                </View>
                <View style={styles.TabContainerStyle}>
                    <Button
                        insideStyle={styles.insideButtonStyle}
                        style={styles.buttonStyle}>

                        <Icon name="md-settings" style={styles.footerFontStyle}/>
                        <Text style={styles.footerFontStyle}>Settings</Text>
                    </Button>
                    <View style={styles.line}/>
                    <Button
                        insideStyle={styles.insideButtonStyle}
                        style={styles.buttonStyle}
                        onPress={() => {
                            this.props.LogoutUser();
                        }}>
                        <Icon name="md-log-out" style={styles.footerFontStyle}/>
                        <Text style={styles.footerFontStyle}>Log out</Text>
                    </Button>
                </View>
            </View>
        );
    }

}


const mapStateToProps = ({auth}) => ({...auth});

SideBar.propTypes = {
    LogoutUser: PropTypes.func,
    navigation: PropTypes.instanceOf(Object),
    user: PropTypes.instanceOf(Object)
};

const SideBarRedux = connect(mapStateToProps, {
    LogoutUser,
})(SideBar);
export {SideBarRedux as SideBar};
