import React from "react";
import {ActivityIndicator, View} from "react-native";
import PropTypes from "prop-types";

const ListFooterComponent = ({loading}) => {
    if (!loading) return null;

    return (
        <View
            style={{
                paddingVertical: 20,
                borderTopWidth: 1,
                borderColor: "#CED0CE"
            }}>
            <ActivityIndicator animating size="large"/>
        </View>
    );
};

ListFooterComponent.propTypes = {
    loading: PropTypes.bool
};

export default ListFooterComponent;
