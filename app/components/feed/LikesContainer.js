/* eslint-disable import/no-unresolved */
import React from "react";
import {Image, Platform, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import PropTypes from "prop-types";
import {withNavigation} from "react-navigation";

const styles = StyleSheet.create({
    avatarImageMask: {
        width: 36,
        height: 36,
        borderRadius: Platform.select({
            ios: 18,
            android: 36
        }),
        backgroundColor: "#000",
        opacity: 0.5,
        zIndex: 2,
        position: "absolute",
        alignItems: "center",
        justifyContent: "center",

    },
    avatarImage: {
        width: 36,
        height: 36,
        borderRadius: Platform.select({
            ios: 18,
            android: 36
        }),
    },
    avatarImageContainer: {
        width: 36,
        height: 36,
        borderRadius: Platform.select({
            ios: 18,
            android: 36
        }),
        marginLeft: -15,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#ccc",
        backgroundColor: "#ddd"
    },
    likerContainer: {
        flexDirection: "row",
        alignItems: "flex-end",
    },
});

const LikesContainer = ({likes, navigation}) => (
    <TouchableOpacity
        style={styles.likerContainer}
        onPress={() => {
            navigation.navigate("FeedLikes", {
                likes
            });
        }}>
        {likes.map((like, index) => {
            if (index === likes.length - 1 || index < 3) {
                return (
                    <View
                        key={index.toString()}
                        style={[styles.avatarImageContainer, {marginLeft: index === 0 ? 0 : -20}]}>
                        <Image
                            style={styles.avatarImage}
                            source={like.user.profile.photo ? {uri: like.user.profile.photo} : require("../../../assets/icons/avatar_placeholder.png")}
                        />
                        {index === likes.length - 1 &&
                        <View style={styles.avatarImageMask}>
                            <Text style={{color: "#fff"}}>{`${likes.length >= 99 ? "+99" : likes.length}`}</Text>
                        </View>
                        }
                    </View>
                );
            }

            return null;
        })}
    </TouchableOpacity>
);

LikesContainer.propTypes = {
    likes: PropTypes.instanceOf(Object),
    navigation: PropTypes.instanceOf(Object)
};

export default withNavigation(LikesContainer);
