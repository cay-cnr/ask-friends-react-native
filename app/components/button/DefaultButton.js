import React from "react";
import {Text, View} from "react-native";
import PropTypes from "prop-types";
import {Button} from "./common";
import colorConfig from "../../../configs/colorConfig";

const styles = {
    defaultDimension: {
        height: 48,
        width: "auto",
        minWidth: 100
    },
    container: {
        backgroundColor: "#fff",
    },
    title: {
        color: colorConfig().DEFAULT_BUTTON_COLOR,
    },
    titleContainer: {
        alignItems: "center",
        justifyContent: "center",
    }
};

const DefaultButton = ({
    onPress,
    title,
    style,
    rippleColor,
    titleStyle
}) => (
    <Button
        onPress={onPress}
        style={[styles.defaultDimension, styles.container, style]}
        color={rippleColor || colorConfig().DEFAULT_RIPPLE_COLOR}>
        <View style={[styles.defaultDimension, styles.titleContainer, style]}>
            <Text style={[styles.title, titleStyle]}>
                {title}
            </Text>
        </View>
    </Button>
);

DefaultButton.propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.instanceOf(Object),
    titleStyle: PropTypes.instanceOf(Object),
    title: PropTypes.string,
    rippleColor: PropTypes.string
};

export {DefaultButton};
