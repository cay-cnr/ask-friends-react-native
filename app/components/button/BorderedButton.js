import {Button} from "./common";
import React from "react";
import {Text, View} from "react-native";
import colorConfig from "../../../configs/colorConfig";

const BorderedButton = ({onPress, title, style, rippleColor, titleStyle}) => {
    return (
        <Button
            onPress={onPress}
            style={[styles.defaultDimension, styles.container, style]}
            rippleColor={rippleColor || colorConfig().DEFAULT_RIPPLE_COLOR}
        >
            <View style={[styles.titleContainer,]}>
                <Text style={[styles.title, titleStyle]}>
                    {title}
                </Text>
            </View>
        </Button>
    )
};

export {BorderedButton};

const styles = {
    defaultDimension: {
        height: 48,
        width: 'auto'
    },
    container: {
        backgroundColor: 'transparent',
        borderColor: colorConfig().DEFAULT_BUTTON_COLOR,
        borderWidth: 1
    },
    title: {
        color: colorConfig().DEFAULT_BUTTON_COLOR,
        margin: 16

    },
    titleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    }
};