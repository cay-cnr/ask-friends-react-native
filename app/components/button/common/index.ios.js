import {TouchableOpacity} from "react-native";
import React from "react";
import PropTypes from "prop-types";

const Button = ({
    children, onPress, style, insideStyle
}) => (
    <TouchableOpacity
        activeOpacity={0.9}
        onPress={onPress}
        style={[style, insideStyle]}>
        {children}
    </TouchableOpacity>
);

Button.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    onPress: PropTypes.func,
    style: PropTypes.instanceOf(Object),
    insideStyle: PropTypes.instanceOf(Object),
};


export {Button};
