import {TouchableNativeFeedback, View} from "react-native";
import React from "react";
import PropTypes from "prop-types";
import colorConfig from "../../../../configs/colorConfig";

const styles = {
    buttonStyle: {
        width: "100%",
    }
};

const Button = ({
    children, onPress, rippleColor, style, insideStyle
}) => (
    <View style={style}>
        <TouchableNativeFeedback
            onPress={onPress}
            background={TouchableNativeFeedback.Ripple(rippleColor || colorConfig().DEFAULT_RIPPLE_COLOR, true)}
            useForeground={false}
            borderless>
            <View style={[styles.buttonStyle, insideStyle]}>
                {children}
            </View>
        </TouchableNativeFeedback>
    </View>
);

Button.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    onPress: PropTypes.func,
    style: PropTypes.instanceOf(Object),
    rippleColor: PropTypes.string,
    insideStyle: PropTypes.instanceOf(Object)
};


export {Button};
