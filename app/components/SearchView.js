/* eslint-disable import/no-unresolved */
import React, {Component} from "react";
import PropTypes from "prop-types";
import {Image, TextInput, View} from "react-native";

const styles = {
    searchImage: {
        position: "absolute",
        marginTop: 12,
        width: 16,
        height: 16,
        right: 15
    }
};

class SearchView extends Component {
    static defaultProps = {
        onChange() {
        },
        caseSensitive: false,
        fuzzy: false,
        throttle: 100
    };

    constructor(props) {
        super(props);
        this.state = {
            searchTerm: this.props.value || ""
        };
    }

    componentWillReceiveProps(nextProps) {
        if (typeof nextProps.value !== "undefined" && nextProps.value !== this.props.value) {
            const e = {
                target: {
                    value: nextProps.value
                }
            };
            this.updateSearch(e);
        }
    }

    updateSearch = (e) => {
        const searchTerm = e.target.value;
        this.setState({
            searchTerm
        }, () => {
            if (this._throttleTimeout) {
                clearTimeout(this._throttleTimeout);
            }

            this._throttleTimeout = setTimeout(
                () => this.props.onChange(searchTerm),
                this.props.throttle
            );
        });
    };

    render() {
        const {
            style, ...inputProps
        } = this.props;

        inputProps.type = inputProps.type || "search";
        inputProps.value = this.state.searchTerm;
        inputProps.onChange = this.updateSearch;
        inputProps.placeholder = inputProps.placeholder || "Search";
        return (
            <View style={{
                margin: 10,
                backgroundColor: "#eeeeee",
                borderRadius: 9,
                paddingLeft: 15,
            }}>
                <TextInput
                    underlineColorAndroid="rgba(0,0,0,0)"
                    style={style}
                    {...inputProps} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                />

                <Image
                    source={require("../../assets/icons/search_icon.png")}
                    style={styles.searchImage}
                />
            </View>
        );
    }
}

SearchView.propTypes = {
    onChange: PropTypes.func,
    caseSensitive: PropTypes.bool,
    sortResults: PropTypes.bool,
    fuzzy: PropTypes.bool,
    throttle: PropTypes.number,
    value: PropTypes.string,
    style: PropTypes.instanceOf(Object)
};

export {SearchView};
