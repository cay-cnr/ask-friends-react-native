import API from "../API";
import {FEEDS_LOADED, LIKED_FEED, NEW_FEEDS_LOADED_END, UNLIKED_FEED} from "./actionTypes";

export const getFeeds = (url) => (async (dispatch) => new Promise(async (resolve, reject) => {
    const response = await API.getFeeds(url);

    if (!response.error) {
        if (!url) {
            dispatch({
                type: FEEDS_LOADED,
                payload: response
            });
        }
        else {
            dispatch({
                type: NEW_FEEDS_LOADED_END,
                payload: response
            });
        }

        resolve();
    }
    else {
        reject(response.error);
    }
}));

export const likeFeed = (feedId) => (async (dispatch) => {
    const response = await API.likeFeed({
        feed: feedId,
        activity_type: "L"
    });

    if (!response.error) {
        dispatch({
            type: LIKED_FEED,
            payload: response,
            feedId
        });
    }
});

export const unlikeFeed = (activityId, feedId) => (async (dispatch) => {
    const response = await API.unlikeFeed(activityId);

    if (response.status) {
        dispatch({
            type: UNLIKED_FEED,
            activityId,
            feedId
        });
    }
});
