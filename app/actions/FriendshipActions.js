import API from "../API";
import {FRIENDSHIP_LIST_UPDATED} from "./actionTypes";

export const getFriendList = () => (async (dispatch) => {
    const response = await API.friends();
    if (!response.error) {
        dispatch({
            type: FRIENDSHIP_LIST_UPDATED,
            payload: response,
        });
    }

});
