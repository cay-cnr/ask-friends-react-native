import API from "../API";
import {QUESTION_CREATE, QUESTION_UPDATE, QUIZ_CREATE, QUIZ_UPDATE, QUIZIES_LOADED} from "./actionTypes";

export const getQuizies = () => (async (dispatch) => {
    return new Promise(async (resolve) => {
        const response = await API.getQuizies();

        dispatch({
            type: QUIZIES_LOADED,
            payload: response
        });

        resolve(response);
    });
});

export const createOrUpdateQuiz = (item, update = false) => ((dispatch) => {
    API.createOrUpdateQuiz(item, update)
        .then((response) => {
            dispatch({
                type: update ? QUIZ_UPDATE : QUIZ_CREATE,
                payload: response
            });
        });
});

export const getQuestions = (quiz_id) => (async (dispatch) => {
    const response = await API.getQuestions(quiz_id);
    if (response) {
        dispatch({
            type: QUIZIES_LOADED,
            payload: response
        });
    }
});


export const createOrUpdateQuestion = (item, update = false) => (async (dispatch) => {
    const response = await API.createOrUpdateQuestion(item, update);
    // FIXME look come to response error or success
    if (response) {
        dispatch({
            type: update ? QUESTION_UPDATE : QUESTION_CREATE,
        });
    }
});
