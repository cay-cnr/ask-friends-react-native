export const UPDATED_USER = "updated_user";
export const LOGOUT_USER = "logout_user";
export const LOGGED_STATUS = "logged_status";
export const AUTH_USER = "authenticated_user";
export const AUTH_TOKEN = "authenticate_token";


export const QUIZIES_LOADED = "quizies_loaded";
export const QUESTIONS_LOADED = "questions_loaded";
export const QUIZ_UPDATE = "quiz_update";
export const QUESTION_UPDATE = "question_update";
export const QUIZ_CREATE = "quiz_create";
export const QUESTION_CREATE = "question_create";

/**
 * Feeds load from server
 * @type {string}
 */
export const FEEDS_LOADED = "feeds_loaded";
export const LIKED_FEED = "liked_feed";
export const UNLIKED_FEED = "unliked_feed";
export const NEW_FEEDS_LOADED_END = "new_feed_loaded_end";


// when friendship list updated go use this key and payload friends json
export const FRIENDSHIP_LIST_UPDATED = "friendship_list_updated";
