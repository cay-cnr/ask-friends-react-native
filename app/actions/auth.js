import {AsyncStorage} from "react-native";
import {NavigationActions} from "react-navigation";

import API from "../API";
import {AUTH_TOKEN, AUTH_USER, UPDATED_USER} from "./actionTypes";

export const UPDATE_USER = (user) => {
    AsyncStorage.setItem(AUTH_USER, JSON.stringify(user));

    return {
        type: UPDATED_USER,
        payload: user
    };
};

export const LoginUser = (data) => (async (dispatch) => {
    const response = await API.Login(data);

    if (response.token) {
        try {
            AsyncStorage.setItem(AUTH_TOKEN, response.token);
            // Add token to API
            API.token = response.token;
            // Update store user, logged, nav
            dispatch(UPDATE_USER(response.user));

            dispatch(NavigationActions.navigate({
                routeName: "App"
            }));
        }
        catch (err) {
            // console.log("err", err);
        }
    }
});

export const LogoutUser = () => ((dispatch) => {
    AsyncStorage.removeItem(AUTH_TOKEN);
    dispatch(NavigationActions.navigate({
        routeName: "Login"
    }));
});

export const UserIsAuthenticated = () => async (dispatch) => {

    const authUser = await AsyncStorage.getItem(AUTH_USER);
    const token = await AsyncStorage.getItem(AUTH_TOKEN);
    dispatch(UPDATE_USER(JSON.parse(authUser)));

    API.token = token;

    // console.log("token", token);
    // console.log("authUser", authUser);


    dispatch(NavigationActions.navigate({
        routeName: (token && "App") || "Auth"
    }));
};
