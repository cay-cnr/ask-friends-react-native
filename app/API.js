import {API_URL} from "../configs/colorConfig";

class APIS {
    token = false;

    defaultData = ({data = "", method = "POST"}) => {
        const defaultData = {
            headers: {
                "Content-Type": "application/json",
            },
            method,
        };

        if (this.token) {
            defaultData.headers["Authorization"] = `Token ${this.token}`;
        }

        if (method !== "GET" && method !== "HEAD" && method !== "DELETE") {
            defaultData["body"] = data && JSON.stringify(data);
        }

        return defaultData;
    };

    Login = (data) => fetch(`${API_URL}/auth/api-token-auth/`, this.defaultData({data}))
        .then((response) => response.json())
        .catch((error) => error);

    getQuizies = () => fetch(`${API_URL}/quizies/`, this.defaultData({method: "GET"}))
        .then((response) => response.json())
        .catch((error) => error);

    createOrUpdateQuiz = (quiz, update = false) => {
        let url = `${API_URL}/quizies/`;
        if (update) {
            url += quiz.id.toString();
        }

        return fetch(url, this.defaultData({
            data: quiz,
            method: update ? "PUT" : "POST"
        }))
            .then((response) =>
                // console.log(response);
                response.json())
            .catch((error) => error);
    };


    createOrUpdateQuestion = (question, update = false) => {
        // TODO buraya quiz id si geliyormu Question ın içinde gelmiyorsa ektra Quiz id sini gönder API yede create update question endpoıntlerini ekle

        let url = `${API_URL}/quizies/`;
        if (update) {
            url += question.id.toString();
        }

        // console.log(url);
        return fetch(url, this.defaultData({
            data: question,
            method: update ? "PUT" : "POST"
        }))
            .then((response) =>
                // console.log(response);
                response.json())
            .catch((error) => error);
    };

    /**
     *
     * get friend list via API
     * @returns {Promise<any>}
     */

    friends = () => fetch(`${API_URL}/friendship/friends/`, this.defaultData({method: "GET"}))
        .then((response) => response.json())
        .catch((error) => error);

    /**
     * get question list from quiz_id via API
     *
     * @param quizId
     * @returns {Promise<any>}
     */

    getQuestions = (quizId) => fetch(`${API_URL}/quizies/${quizId}/questions/`, this.defaultData({method: "GET"}))
        .then((response) => response.json());

    getFeeds = (url = `${API_URL}/feed/`) => fetch(url, this.defaultData({method: "GET"}))
        .then((response) => response.json());

    likeFeed = (data) => fetch(`${API_URL}/activity/`, this.defaultData({data}))
        .then((response) => response.json());

    unlikeFeed = (activityId) => {
        console.log(`${API_URL}/activity/${activityId}/delete`);
        console.log(this.defaultData({method: "DELETE"}));

        return fetch(`${API_URL}/activity/${activityId}/delete`, this.defaultData({method: "DELETE"}))
            .then((response) => response.json());
    };
}

export default new APIS();
