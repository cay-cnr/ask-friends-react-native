import './ReactotronConfig'
import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('ask_friends_react', () => App);
