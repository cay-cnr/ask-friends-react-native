import {Feeds, FriendList, Profile, QuizList} from "../app/screens";

export default () => ({
    /**
     *npm
     * Load Route Page
     */

    ROUTE_PAGES: [{
        name: "Feeds",
        routeName: "Feeds",
        drawer_component: true,
        screen: Feeds
    }, {
        name: "Quiz List",
        routeName: "QuizList",
        drawer_component: true,
        screen: QuizList
    }, {
        name: "Friends",
        routeName: "FriendList",
        drawer_component: true,
        screen: FriendList
    }, {
        name: "LeaderBoard",
        routeName: "Home",
        drawer_component: true,
        screen: Feeds
    }, {
        name: "Notifications",
        routeName: "Notifications",
        drawer_component: true,
        screen: Feeds
    }, {
        name: "Profile",
        routeName: "Profile",
        screen: Profile
    }
    ]
});


