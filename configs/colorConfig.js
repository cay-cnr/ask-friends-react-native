export const API_URL = "http://askfriends.canercay.com";
export default () => ({

    /**
         *
         * Background Color
         */

    DEFAULT_BACKGROUND_COLOR: "#fbfbfb",

    DEFAULT_RIPPLE_COLOR: "#ccc",

    DEFAULT_BUTTON_COLOR: "#543361",
});
