import Reactotron, {
	trackGlobalErrors,
	openInEditor,
	overlay,
	asyncStorage,
	networking
} from "reactotron-react-native";
import { reactotronRedux } from "reactotron-redux";

Reactotron
	.configure({
		name: "React Native Demo"
	})
	.use(trackGlobalErrors())
	.use(openInEditor())
	.use(overlay())
	.use(reactotronRedux())
	.use(asyncStorage())
	.use(networking())
	.connect();

export default Reactotron;