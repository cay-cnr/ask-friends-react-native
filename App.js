/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from "react";
import {Provider} from 'react-redux';
import configureStore from "./app/store";
import Router from "./app/Router";
import {Text, View} from "react-native";

class MyApp extends React.Component {
    store = configureStore();

    render() {
        if (this.store)
            return (
                <Provider store={this.store}>
                    <Router/>
                </Provider>
            );
        else
            return (
                <View>
                    <Text>auch</Text>
                </View>
            )
    }
}


export default MyApp;